clear_all: clear_containers clear_images clear_networks ## Clear containers and images

clear_containers: ## Stop and clear containers
	$(BIN_DOCKER) stop `$(BIN_DOCKER) ps -a -q` && $(BIN_DOCKER) rm `$(BIN_DOCKER) ps -a -q`

clear_images: ## Clear images
	$(BIN_DOCKER) rmi -f `$(BIN_DOCKER) images -q`

clear_networks: ## Clear network interfaces
	$(BIN_DOCKER) network prune --force

pull: ## Pull docker-compose.yml images
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_UP) pull

build: ## Build docker-compose.yml
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_UP) build

build_ci: ## Build compose-ci.yml
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) build

up: ## Bring up docker-compose.yml containers in background
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_UP) up -d

reload_php: ## Reload PHP-FPM in php container
	$(BIN_DOCKER) exec -it $(CONTAINER_PHP) service php$(PHP_VERSION)-fpm reload

reload_nginx: ## Reload Nginx in nginx container
	$(BIN_DOCKER) exec -it $(CONTAINER_NGINX) service nginx reload

connect_mailcatcher: ## Bash shell on mailcatcher
	$(BIN_DOCKER) exec -it $(CONTAINER_MAILCATCHER) bash

connect_mariadb: ## Bash shell on mariadb
	$(BIN_DOCKER) exec -it $(CONTAINER_MARIADB) bash

connect_nginx: ## Bash shell on nginx
	$(BIN_DOCKER) exec -it $(CONTAINER_NGINX) bash

connect_php: ## Bash shell on php
	$(BIN_DOCKER) exec -it $(CONTAINER_PHP) bash

connect_redis: ## Bash shell on php
	$(BIN_DOCKER) exec -it $(CONTAINER_PHP) bash
