<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('walk', 'HomeController@walk');

Route::middleware(['auth', 'can:role-admin'])
    ->prefix('admin')
    ->namespace('Admin')
    ->group(function () {
        Route::prefix('subnet')
            ->namespace('Subnet')
            ->group(function () {
                \Smorken\Support\Routes::create();
            });
    });