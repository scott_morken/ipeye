## Requirements

- PHP 7.2+
- [Composer](https://getcomposer.org/)
- [Node.js](http://nodejs.org/) and [Node Package Manager](https://www.npmjs.org/)

### Building and running via docker __(DEV ONLY)__

- copy the `docker.copy` directory

```shell script
$ cp -a docker.copy/. docker.project/
```

- build the project and its dependencies

```shell script
$ cd docker.project
$ make publish # publish_dev to build with dev dependencies
```

- copy the `.env.example` to `.env` and edit it to suit your environment

```shell script
$ cp .env.example .env
$ nano .env
```

- run database migrations and seeding
```shell script
$ cd docker.project
$ make connect_php
<Container>
/app # php artisan migrate --seed 
```

- add yourself as a super admin
```shell script
$ cd docker.project
$ make connect_php
<Container>
/app # php artisan role:set 12345678 # your user id
```

- start the docker containers
```shell script
$ cd docker.project
$ docker-compose up # add -d to background the containers
```

- useful make commands

```shell script
$ cd docker.project
$ make help # show all make commands
$ make clear_containers # clear all running and old containers
$ make clear_networks # clear docker created networks
$ sudo make chown # reset the project ownership to your uid:gid
$ make composer_up_dev # composer update (with require-dev)
$ make composer_up # composer update (without require-dev)
$ make npm_install # npm install
$ make webpack_dev # npm run dev
$ make webpack # npm run prod
$ make connect_php # connect to the php-fpm container
```

## License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
