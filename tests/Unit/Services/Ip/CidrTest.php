<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 2:35 PM
 */

namespace Tests\App\Unit\Services\Ip;

use App\Services\Ip\Cidr;

class CidrTest extends \PHPUnit\Framework\TestCase
{

    public function testInvalidIpIsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $sut = $this->getSut('foo/24');
    }

    public function testNoMaskIsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $sut = $this->getSut('127.0.0.1');
    }

    public function testInvalidMaskIsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $sut = $this->getSut('127.0.0.1/500');
    }

    public function testInvalidMaskAsZeroIsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $sut = $this->getSut('127.0.0.1/0');
    }

    public function testGetNetworkSingleSubnet()
    {
        $sut = $this->getSut('127.0.0.10/24');
        $this->assertEquals('127.0.0.0', $sut->getNetwork());
    }

    public function testGetNetworkMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.0.0', $sut->getNetwork());
    }

    public function testGetFirstSingleSubnet()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $this->assertEquals('10.10.1.1', $sut->getFirst());
    }

    public function testGetFirstMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.0.1', $sut->getFirst());
    }

    public function testGetBroadcastSingleSubnet()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $this->assertEquals('10.10.1.255', $sut->getBroadcast());
    }

    public function testGetBroadcastMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.255.255', $sut->getBroadcast());
    }

    public function testGetFirstAndLastMultiSubnet()
    {
        $sut = $this->getSut('10.10.1.1/16');
        $this->assertEquals('10.10.0.1', $sut->getFirst());
        $this->assertEquals('10.10.255.254', $sut->getLast());
    }

    public function testGetBroadcastOddSubnet()
    {
        $sut = $this->getSut('10.10.1.1/32');
        $this->assertEquals('10.10.1.1', $sut->getBroadcast());
    }

    public function testGetRangeWithOddSubnetIsOne()
    {
        $sut = $this->getSut('10.10.1.1/32');
        $r = [];
        foreach ($sut->getRange() as $ip) {
            $r[] = $ip;
        }
        $this->assertCount(1, $r);
    }

    public function testGetRangeNormal()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $r = [];
        foreach ($sut->getRange() as $ip) {
            $r[] = $ip;
        }
        $this->assertCount(254, $r);
    }

    public function testNewInstanceIsNew()
    {
        $sut = $this->getSut('10.10.1.1/24');
        $new = $sut->newInstance('192.168.0.1/24');
        $expected = [
            'ip'        => '192.168.0.1',
            'netmask'   => '255.255.255.0',
            'network'   => '192.168.0.0',
            'broadcast' => '192.168.0.255',
        ];
        $this->assertEquals($expected, $new->asArray());
    }

    /**
     * @return Cidr
     */
    protected function getSut($subnet)
    {
        return new Cidr($subnet);
    }
}
