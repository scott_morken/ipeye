<?php


namespace Tests\App\Integration\Observers;


use App\Models\Eloquent\Ip;
use App\Observers\HistoryObserver;
use App\Storage\Eloquent\HistoricalIp;
use Carbon\Carbon;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Processors\MySqlProcessor;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class HistoryObserverTest extends TestCase
{

    protected $cr;
    protected $conn;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupDb();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testShouldUpdateIsFalseForNoCurrentAndPreviousLessThanOneHour()
    {
        [$sut, $hip, $m] = $this->getSut();
        $now = Carbon::now();
        $current = null;
        $previous = $now->copy()->subSeconds(360);
        $this->setOnModel($m, 'last_response', $current, $previous);
        $this->assertFalse($sut->updating($m));
    }

    public function testShouldUpdateIsTrueForNoCurrentAndPreviousGreaterThanOneHour()
    {
        [$sut, $hip, $m] = $this->getSut();
        $now = Carbon::now();
        $current = null;
        $previous = $now->copy()->subSeconds(7200);
        $this->setOnModel($m, 'last_response', $current, $previous);
        $this->setOnModel($m, 'created_at', $now, $now);
        $this->setOnModel($m, 'updated_at', $now, $now);
        $this->assertTrue($sut->updating($m));
    }

    public function testNewModelDoesntNeedHistory()
    {
        [$sut, $hip, $m] = $this->getSut();
        $now = Carbon::now();
        $this->setOnModel($m, 'last_response', null, null);
        $this->setOnModel($m, 'created_at', $now, $now);
        $this->setOnModel($m, 'updated_at', $now, $now);
        $this->assertTrue($sut->updating($m));
    }

    public function testNewModelNullDatesDoesntNeedHistory()
    {
        [$sut, $hip, $m] = $this->getSut();
        $this->setOnModel($m, 'last_response', null, null);
        $this->setOnModel($m, 'created_at', null, null);
        $this->setOnModel($m, 'updated_at', null, null);
        $this->assertTrue($sut->updating($m));
    }

    public function testNewModelSmallDateDiffDoesntNeedHistory()
    {
        [$sut, $hip, $m] = $this->getSut();
        $now = Carbon::now();
        $created = $now->copy()->subSeconds(2);
        $this->setOnModel($m, 'last_response', null, null);
        $this->setOnModel($m, 'created_at', $created, $created);
        $this->setOnModel($m, 'updated_at', $now, $now);
        $this->assertTrue($sut->updating($m));
    }

    public function testModelAddsHostnameNeedsHistory()
    {
        [$sut, $hip, $m] = $this->getSut();
        $now = Carbon::now();
        $created = $now->copy()->subHour();
        $updated = $now->copy()->addMinutes(30);
        $this->setOnModel($m, 'created_at', $created, $created);
        $this->setOnModel($m, 'updated_at', $updated, $updated);
        $this->setOnModel($m, 'last_response', $created, $updated);
        $this->setOnModel($m, 'hostname', 'foo.bar.com', null);
        $this->mockInsert('insert into `historical_ips` (`ip_id`, `hostname`, `last_response`, `response`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?)');
        $this->assertTrue($sut->updating($m));
    }

    public function testNewResponseNeedsHistory()
    {
        [$sut, $hip, $m] = $this->getSut();
        $now = Carbon::now();
        $created = $now->copy()->subHour();
        $updated = $now->copy()->addMinutes(30);
        $this->setOnModel($m, 'created_at', $created, $created);
        $this->setOnModel($m, 'updated_at', $updated, $updated);
        $this->setOnModel($m, 'hostname', 'foo.bar.com', 'foo.bar.com');
        $m->last_response = $created;
        $this->mockInsert('insert into `historical_ips` (`ip_id`, `hostname`, `last_response`, `response`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?)');
        $this->assertTrue($sut->updating($m));
    }

    protected function setOnModel($model, $attribute, $current, $original)
    {
        $model->setAttribute($attribute, $original);
        $model->syncOriginal();
        $model->setAttribute($attribute, $current);
    }

    protected function getSut()
    {
        $hip = new HistoricalIp(new \App\Models\Eloquent\HistoricalIp());
        $ip = new IpStub(['id' => '127.0.0.1']);
        $sut = new HistoryObserver($hip);
        return [$sut, $hip, $ip];
    }

    protected function mockInsert($sql)
    {
        $this->conn->shouldReceive('insert')->once()->with($sql, m::type('array'))->andReturn(true);
        $pdo = m::mock(\PDO::class);
        $pdo->shouldReceive('lastInsertId')->once()->andReturn(99);
        $this->conn->shouldReceive('getName')->once()->andReturn('db');
        $this->conn->shouldReceive('getPdo')->once()->andReturn($pdo);
    }

    protected function setupDb()
    {
        $grammar = new MySqlGrammar();
        $this->conn = m::mock(ConnectionInterface::class);
        $this->conn->shouldReceive('getQueryGrammar')->andReturn($grammar);
        $this->conn->shouldReceive('getPostProcessor')->andReturn(new MySqlProcessor());
        $this->cr = $this->getMockBuilder(ConnectionResolver::class)
            ->onlyMethods(['connection'])
            ->getMock();
        $this->cr->method('connection')->willReturn($this->conn);
        \App\Models\Eloquent\HistoricalIp::setConnectionResolver($this->cr);
        IpStub::setConnectionResolver($this->cr);
        date_default_timezone_set('UTC');
    }
}

class IpStub extends Ip implements \App\Contracts\Models\Ip
{
    public $timestamps = false;
}