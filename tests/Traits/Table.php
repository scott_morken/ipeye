<?php


namespace Tests\App\Traits;


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

trait Table
{
    protected function createTableFromAttributes(string $table, array $attributes, array $additional = []): void
    {
        Schema::create($table, function (Blueprint $t) use ($attributes, $additional) {
            foreach ($attributes as $k) {
                $t->string($k)
                    ->nullable();
            }
            $t->timestamps();
            foreach ($additional as $cmd) {
                call_user_func_array([$t, $cmd[0]], $cmd[1] ?? []);
            }
        });
    }

    protected function createTableFromModelClass(string $table, string $model, array $additional = []): void
    {
        $m = factory($model)->make();
        $this->createTableFromModel($table, $m, $additional);
    }

    protected function createTableFromModel(string $table, object $model, array $additional = []): void
    {
        $this->createTableFromAttributes($table, array_keys($model->getAttributes()));
    }

    protected function deleteTable(string $table)
    {
        Schema::dropIfExists($table);
    }
}
