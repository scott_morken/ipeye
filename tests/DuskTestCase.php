<?php

namespace Tests\App;

use Dotenv\Dotenv;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Illuminate\Support\Facades\Auth;
use Laravel\Dusk\TestCase as BaseTestCase;
use Smorken\Auth\Proxy\AuthProvider;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Contracts\Models\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Symfony\Component\HttpFoundation\File\Exception\CannotWriteFileException;
use Tests\App\Traits\CreatesApplication;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Run migrate:fresh once per test suite run
     * @var bool
     */
    protected static $migrationsRun = false;

    /**
     * Run $this->createTables() once per test suite run
     * @var bool
     */
    protected static $tablesCreated = false;

    /**
     * Run $this->seedForTest() once per test suite run OR
     * if $this->shouldDelete is true
     * @var bool
     */
    protected static $testSeedsRun = false;

    protected $retries = 5;

    /**
     * On test destroy, rerun migrations
     * if $this->shouldDelete ? migrate:fresh : migrate:refresh
     * @var bool
     */
    protected $shouldRefresh = true;

    /**
     * Run $this->runDatabaseMigrations()
     * @var bool
     */
    protected $runMigrations = true;

    /**
     * Run $this->seed()
     * @var bool
     */
    protected $shouldSeed = true;

    /**
     * On test destroy, run $this->deleteTablesForTest
     * Also controls type of test destroy migration and whether
     * to reseed for test
     * @var bool
     */
    protected $shouldDelete = true;

    public static function basePath($path = '')
    {
        return __DIR__.'/../'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    public static function setUpBeforeClass(): void
    {
        register_shutdown_function(function () {
            self::tryCopy(self::basePath('.env.backup'), self::basePath('.env'));
        });
        if (!file_exists(self::basePath('.env.original'))) {
            self::tryCopy(self::basePath('.env'), self::basePath('.env.original'));
        }
        self::tryCopy(self::basePath('.env'), self::basePath('.env.backup'));
        self::tryCopy(self::basePath('.env.dusk.local'), self::basePath('.env'));
        $dotenv = Dotenv::create(self::basePath());
        $dotenv->overload();
        parent::setUpBeforeClass();
    }

    protected static function tryCopy($from, $to)
    {
        $continue = true;
        if (file_exists($to)) {
            $fromstr = file_get_contents($from);
            $tostr = file_get_contents($to);
            $continue = $fromstr !== $tostr;
        }
        if ($continue) {
            if (!copy($from, $to)) {
                throw new CannotWriteFileException("Cannot write [$from] to [$to].");
            }
        }
    }

    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations()
    {
        if ($this->runMigrations === false) {
            return;
        }
        if (static::$migrationsRun === false) {
            $this->artisan('migrate:fresh');
            static::$migrationsRun = true;
        }
        if (static::$tablesCreated === false) {
            $this->createTables();
            static::$tablesCreated = true;
        }
        $this->createTablesForTest();

        $this->beforeApplicationDestroyed(function () {
            if ($this->shouldRefresh) {
                $cmd = 'migrate:'.($this->shouldDelete ? 'fresh' : 'refresh');
                $this->artisan($cmd);
            }
            if ($this->shouldDelete) {
                $this->deleteTablesForTest();
            }
        });
        $this->seedTables();
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->runDatabaseMigrations();
        $this->beforeApplicationDestroyed(function () {
            self::tryCopy(self::basePath('.env.backup'), self::basePath('.env'));
        });
    }

    protected function checkEndpoints(array $urls)
    {
        foreach ($urls as $url) {
            if (!$this->waitForEndpoint($url['url'], $url['callback'])) {
                throw new Exception($url['url'].' is not responding.');
            }
        }
    }

    protected function createTables()
    {
        return;
    }

    protected function createTablesForTest()
    {
        return;
    }

    protected function deleteTablesForTest()
    {
        return;
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return RemoteWebDriver
     * @throws Exception
     */
    protected function driver()
    {
        $this->checkEndpoints($this->getUrls());
        $url = env('DUSK_BROWSER_URL', 'http://localhost:9515');
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--window-size=1920,1080',
            '--ignore-ssl-errors',
            '--no-sandbox',
            '--whitelisted-ips',
        ]);
        return RemoteWebDriver::create($url, DesiredCapabilities::chrome()
            ->setCapability(ChromeOptions::CAPABILITY, $options)
            ->setCapability(WebDriverCapabilityType::ACCEPT_SSL_CERTS,
                true)
            ->setCapability('acceptInsecureCerts', true));
    }

    protected function getResponse($url)
    {
        $c = curl_init();
        curl_setopt_array($c, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ]);
        $r = curl_exec($c);
        curl_close($c);
        return $r;
    }

    protected function getUrls()
    {
        return [
            [
                'url' => env('DUSK_BROWSER_URL', 'http://localhost:9515').'/status',
                'callback' => function ($r) {
                    $response = json_decode($r, true);
                    if ($response && isset($response['value']['ready']) && $response['value']['ready'] === true) {
                        return true;
                    }
                    return false;
                },
            ],
            [
                'url' => env('APP_URL', 'http://web:8000'),
                'callback' => function ($r) {
                    return $r !== false;
                },
            ],
        ];
    }

    protected function seedForTest()
    {
        return;
    }

    protected function seedTables()
    {
        if ($this->shouldSeed) {
            if ($this->shouldRefresh) {
                if (class_exists('TestSeeder')) {
                    $this->seed('TestSeeder');
                }
                if (class_exists(RoleUser::class)) {
                    RoleUser::firstOrCreate(['role_id' => 1, 'user_id' => 1]);
                }
            }
            if (self::$testSeedsRun === false || $this->shouldDelete) {
                $this->seedForTest();
                self::$testSeedsRun = true;
            }
        }
    }

    protected function waitForEndpoint($url, callable $valid, $tries = 0)
    {
        $wait = $tries < $this->retries ? true : false;
        $r = $this->getResponse($url);
        if ($r) {
            if ($valid($r)) {
                return true;
            } else {
                $r = false;
            }
        }
        if ((!$r) && $wait) {
            sleep(1);
            return $this->waitForEndpoint($url, $valid, ++$tries);
        }
        return false;
    }
}
