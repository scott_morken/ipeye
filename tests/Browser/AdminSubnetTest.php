<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Subnet;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminSubnetTest extends DuskTestCase
{

    public function setUp(): void
    {
        if (!file_exists('/app/database/testing.sqlite')) {
            touch('/app/database/testing.sqlite');
        }
        parent::setUp();
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->assertSee('Subnet Administration')
                ->assertSee('No records found');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser()
    {
        $user = factory(User::class)->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecord()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->clickLink('New')
                ->assertPathIs('/admin/subnet/create')
                ->type('subnet', '10.10.10.0/24')
                ->type('descr', 'Test Create')
                ->press('Save')
                ->assertPathIs('/admin/subnet')
                ->assertSee('Test Create');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->clickLink('New')
                ->assertPathIs('/admin/subnet/create')
                ->press('Save')
                ->assertPathIs('/admin/subnet/create')
                ->assertSee('subnet field is required')
                ->assertSee('descr field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteExistingRecord()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = factory(Subnet::class)->create();
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->assertSee($model->subnet)
                ->clickLink('delete')
                ->assertPathIs('/admin/subnet/delete/'.$model->id)
                ->assertSee($model->subnet)
                ->press('Delete')
                ->assertPathIs('/admin/subnet')
                ->assertDontSee($model->subnet);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteMissingRecord()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/subnet/delete/2')
                ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecord()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = factory(Subnet::class)->create();
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->assertSee($model->subnet)
                ->clickLink('update')
                ->assertPathIs('/admin/subnet/update/'.$model->id)
                ->assertInputValue('subnet', $model->subnet)
                ->assertInputValue('descr', $model->descr)
                ->type('descr', 'Test Update')
                ->press('Save')
                ->assertPathIs('/admin/subnet')
                ->assertSee('Test Update');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = factory(Subnet::class)->create();
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->assertSee($model->subnet)
                ->clickLink('update')
                ->assertPathIs('/admin/subnet/update/'.$model->id)
                ->assertInputValue('subnet', $model->subnet)
                ->assertInputValue('descr', $model->descr)
                ->type('subnet', '1000.1000/24')
                ->press('Save')
                ->assertPathIs('/admin/subnet/update/'.$model->id)
                ->assertSee('subnet must be a valid subnet');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateMissingRecord()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/subnet/update/2')
                ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewExistingRecord()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = factory(Subnet::class)->create();
            $browser->loginAs($user)
                ->visit('/admin/subnet')
                ->assertSee($model->subnet)
                ->clickLink('1')
                ->assertPathIs('/admin/subnet/view/'.$model->id)
                ->assertSee($model->subnet);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewMissingRecord()
    {
        $user = factory(User::class)->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/subnet/view/2')
                ->assertSee("The resource you were trying to reach");
        });
    }
}
