<?php

namespace Tests\App\Browser;

use App\Contracts\Storage\Ip;
use App\Models\Eloquent\Subnet;
use App\Services\Ip\Cidr;
use Laravel\Dusk\Browser;
use Tests\App\DuskTestCase;

class HomeTest extends DuskTestCase
{
    /**
     * @return void
     * @throws \Throwable
     */
    public function testNoSubnetSelected()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertSee('Please select a subnet');
        });
    }

    public function testSelectSubnetShowsIpList()
    {
        [$s1, $s2] = $this->seedIps();
        $this->browse(function (Browser $browser) use ($s1, $s2) {
            $c = new Cidr($s1->subnet);
            $c2 = new Cidr($s2->subnet);
            $browser->visit('/')
                ->assertSee('Please select a subnet')
                ->select('subnet_id', $s1->id)
                ->press('Filter')
                ->assertDontSee('Please select a subnet')
                ->with('.ips', function ($b) use ($c, $c2) {
                    $b->assertSee($c->getFirst())
                        ->assertSee($c->getLast())
                        ->assertDontSee($c2->getFirst());
                });
        });
    }

    protected function seedIps()
    {
        $subnet = factory(Subnet::class)->create(['subnet' => '10.1.1.0/24']);
        $cs1 = new Cidr($subnet->subnet);
        $subnet2 = factory(Subnet::class)->create(['subnet' => '10.2.2.0/24']);
        $cs2 = new Cidr($subnet2->subnet);
        $ipp = $this->app[Ip::class];
        $ipp->createInRange($cs1->getRange());
        $ipp->createInRange($cs2->getRange());
        return [$subnet, $subnet2];
    }
}
