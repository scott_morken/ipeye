<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/18
 * Time: 9:19 AM
 */

namespace App\Services;

class Handler implements \App\Contracts\Handler
{

    /**
     * @var \App\Contracts\Ip\Ping|\App\Contracts\Ip\Walk
     */
    protected $handler;

    public function __construct($handler)
    {
        $this->handler = $handler;
    }

    public function getHandler()
    {
        return $this->handler;
    }
}
