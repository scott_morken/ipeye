<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/15/17
 * Time: 8:05 AM
 */

namespace App\Services\Ip\Ping;

use App\Contracts\Ip\Dns;
use App\Contracts\Ip\Ping;

class PhpPing implements Ping
{

    /**
     * @var Dns
     */
    protected $dns;

    public function __construct(Dns $dns)
    {
        $this->dns = $dns;
    }

    public function ping($ip, $lookup_host = false, $count = 1)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            $socket = null;
            $results = $this->getBaseResult($ip, $lookup_host);
            try {
                $socket = $this->getSocket($ip, 1);
                for ($i = 0; $i < $count; $i++) {
                    $results['result'] = $this->send($socket);
                    yield $results;
                }
            } catch (\Exception $e) {
                \Log::warning($e);
                $results['error'] = $e->getMessage();
                yield $results;
            } finally {
                $this->closeSocket($socket);
            }
        } else {
            throw new \InvalidArgumentException("$ip is not a valid IP address.");
        }
    }

    protected function closeSocket($socket)
    {
        if ($socket) {
            socket_close($socket);
        }
        $socket = null;
    }

    protected function getBaseResult($ip, $lookup_host = false)
    {
        $results = [
            'ip'       => $ip,
            'error'    => null,
            'hostname' => false,
            'result'   => false,
        ];
        if ($lookup_host) {
            $results['hostname'] = $this->getHostname($ip);
        }
        return $results;
    }

    protected function getHostname($ip)
    {
        $h = $this->dns->byIp($ip);
        if ($h) {
            return $h;
        }
        return $ip;
    }

    protected function getSocket($ip, $timeout = 1)
    {
        $socket = socket_create(AF_INET, SOCK_RAW, getprotobyname('icmp'));
        /* set socket receive timeout to 1 second */
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ["sec" => $timeout, "usec" => 0]);
        socket_connect($socket, $ip, null);
        return $socket;
    }

    protected function send($socket)
    {
        $pinged = false;
        $pkg = "\x08\x00\x19\x2f\x00\x00\x00\x00\x70\x69\x6e\x67";
        $start = microtime(true);
        socket_send($socket, $pkg, strlen($pkg), 0);
        if (@socket_read($socket, 255)) {
            $pinged = round((microtime(true) - $start) * 1000, 4);
        }
        return $pinged;
    }
}
