<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/15/17
 * Time: 9:22 AM
 */

# @ http://www.askapache.com/php/php-fsockopen-dns-udp/
# Copyright (C) 2013 Free Software Foundation, Inc.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
namespace App\Services\Ip\Dns;

use App\Contracts\Ip\Dns;

class PhpDns implements Dns
{

    protected $servers = ['8.8.8.8'];

    public function __construct(array $servers = ['8.8.8.8'])
    {
        $this->setDnsServers($servers);
    }

    /**
     * @param     $ip
     * @param int $timeout
     * @return null|string
     */
    public function byIp($ip, $timeout = 1)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP) && filter_var($ip, FILTER_VALIDATE_IP)) {
            $socket = null;
            $hostname = null;
            try {
                foreach ($this->getDnsServers() as $server) {
                    $socket = $this->getSocket($server, $timeout);
                    $hostname = $this->lookup($socket, $ip);
                    $this->closeSocket($socket);
                    if ($hostname && $hostname !== $ip) {
                        break;
                    }
                }
            } catch (\Exception $e) {
                \Log::warning($e);
            } finally {
                $this->closeSocket($socket);
            }
            return $hostname ?? $ip;
        } else {
            throw new \InvalidArgumentException("$ip is not a valid IP address.");
        }
    }

    /**
     * @return array
     */
    public function getDnsServers()
    {
        return $this->servers;
    }

    /**
     * @param array $servers
     * @return mixed
     */
    public function setDnsServers(array $servers = ['8.8.8.8'])
    {
        $this->servers = $servers;
    }

    protected function addSegment($response, $position, $len)
    {
        return substr($response, $position + 1, $len) . '.';
    }

    protected function closeSocket($socket)
    {
        if ($socket) {
            socket_close($socket);
        }
        $socket = null;
    }

    protected function getPackage($ip)
    {
        // idea from http://www.php.net/manual/en/function.gethostbyaddr.php#46869
        // http://www.askapache.com/pub/php/gethostbyaddr.php

        // random transaction number (for routers etc to get the reply back)
        $pkg = rand(10, 77) . "\1\0\0\1\0\0\0\0\0\0";

        // octals in the array, keys are strlen of bit
        $bitso = ["", "\1", "\2", "\3"];
        foreach (array_reverse(explode('.', $ip)) as $bit) {
            $l = strlen($bit);
            $pkg .= "{$bitso[$l]}" . $bit;
        }

        // and the final bit of the request
        $pkg .= "\7in-addr\4arpa\0\0\x0C\0\1";
        return $pkg;
    }

    protected function getRawResponse($socket)
    {
        $response = '';
        while ($r = @socket_read($socket, 1024)) {
            $response .= $r;
        }
        return $response;
    }

    protected function getSegmentSize($response, $position)
    {
        return unpack('c', substr($response, $position, 1));
    }

    protected function getSocket($ip, $timeout = 1)
    {
        $socket = socket_create(AF_INET, SOCK_DGRAM, getprotobyname('udp'));
        /* set socket receive timeout to 1 second */
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ["sec" => $timeout, "usec" => 0]);
        socket_connect($socket, $ip, 53);
        return $socket;
    }

    protected function lookup($socket, $ip)
    {
        $pkg = $this->getPackage($ip);
        $pkg_len = strlen($pkg);
        socket_send($socket, $pkg, $pkg_len, 0);
        $response = $this->getRawResponse($socket);
        $r = $this->parseResponse($response, $pkg_len);
        if ($r) {
            return $r;
        }
        return $ip;
    }

    protected function parseResponse($response, $pkg_len)
    {
        // if empty response or bad response, return null
        if (empty($response) || bin2hex(substr($response, $pkg_len + 2, 2)) != '000c') {
            return null;
        }
        // set up our variables
        $host = '';
        $loops = 0;

        // set our pointer at the beginning of the hostname uses the request size from earlier rather than work it out
        $pos = $pkg_len + 12;
        do {
            // get segment size
            $len = $this->getSegmentSize($response, $pos);

            // null terminated string, so length 0 = finished - return the hostname, without the trailing .
            if ($len[1] == 0) {
                return substr($host, 0, -1);
            }

            // add segment to our host
            $host .= $this->addSegment($response, $pos, $len[1]);

            // move pointer on to the next segment
            $pos += $len[1] + 1;

            // recursion protection
            $loops++;
        } while ($len[1] != 0 && $loops < 20);
        return null;
    }
}
