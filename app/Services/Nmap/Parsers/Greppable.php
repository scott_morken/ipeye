<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/18
 * Time: 8:37 AM
 */

namespace App\Services\Nmap\Parsers;

use App\Contracts\Nmap\Response;

class Greppable implements Response
{

    /**
     * @param $response
     * @return bool
     */
    public function isError($response)
    {
        return false;
    }

    /**
     * @param $response
     * @return array
     */
    public function parse($response)
    {
        $ret = [];
        foreach ($response as $line) {
            if (substr($line, 0, 4) === 'Host') {
                preg_match('/Host: ([0-9\.]+)\s+\((.*)\)\s+Status: (\w+)/', $line, $output_array);
                if (count($output_array, 4) && $output_array[1]) {
                    $ret[$output_array[1]] = [
                        'ip'       => $output_array[1],
                        'hostname' => $output_array[2] ?: false,
                        'result'   => $output_array[3] === 'Up',
                    ];
                }
            }
        }
        return $ret;
    }
}
