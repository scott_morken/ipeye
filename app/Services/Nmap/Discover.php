<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/18
 * Time: 8:34 AM
 */

namespace App\Services\Nmap;

use App\Contracts\Ip\Cidr;
use App\Contracts\Nmap\Nmap;
use App\Contracts\Nmap\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class Discover implements Nmap
{

    protected $config = [
        'command'         => '/usr/bin/nmap',
        'additional_args' => '',
    ];

    /**
     * @var \App\Contracts\Nmap\Response
     */
    protected $parser;

    public function __construct(Response $parser, array $config)
    {
        $this->parser = $parser;
        $this->config = array_replace($this->config, $config);
    }

    /**
     * @return \App\Contracts\Nmap\Response
     */
    public function getResponseParser()
    {
        return $this->parser;
    }

    /**
     * @param \App\Contracts\Nmap\Response $response
     * @return void
     */
    public function setResponseParser(Response $response)
    {
        $this->parser = $response;
    }

    /**
     * @param \App\Contracts\Ip\Cidr $cidr
     * @param bool                   $lookup_host
     * @return array|false
     */
    public function walk(Cidr $cidr, $lookup_host = false)
    {
        $cmd = $this->buildCommand($cidr, $lookup_host);
        $responses = $this->createBaseResponses($cidr);
        $err_no = null;
        $results = [];
        $s = exec($cmd, $results, $err_no);
        if ($this->getResponseParser()
                 ->isError($results) || $err_no !== 0) {
            Log::error("Nmap error: $err_no", $results);
            return false;
        }
        $parsed = $this->getResponseParser()
                       ->parse($results);
        return $this->addParsedToResponses($parsed, $responses);
    }

    protected function addParsedToResponses($parsed, $responses)
    {
        foreach ($parsed as $ip => $response) {
            if (isset($responses[$ip][0])) {
                $responses[$ip][0] = array_replace($responses[$ip][0], $response);
            } else {
                $responses[$ip][0] = $response;
            }
        }
        return $responses;
    }

    protected function buildCommand(Cidr $cidr, $lookup_host)
    {
        return escapeshellcmd(
            sprintf(
                '%s%s-oG - -sn %s',
                $this->getOption('command', '/usr/bin/nmap'),
                $this->getAdditionalArgs($lookup_host),
                $this->getSubnet($cidr)
            )
        );
    }

    protected function createBaseResponses(Cidr $cidr)
    {
        $responses = [];
        foreach ($cidr->getRange() as $ip) {
            $responses[$ip] = [
                [
                    'ip'       => $ip,
                    'error'    => null,
                    'hostname' => false,
                    'result'   => false,
                ],
            ];
        }
        return $responses;
    }

    protected function getAdditionalArgs($lookup_host)
    {
        return sprintf(' %s%s', $this->getOption('additional_args'), $lookup_host ? ' ' : ' -n ');
    }

    protected function getOption($key, $default = null)
    {
        return Arr::get($this->config, $key, $default);
    }

    protected function getSubnet(Cidr $cidr)
    {
        return $cidr->getSubnet();
    }
}
