<?php

namespace App\Providers;

use App\Contracts\Handler;
use App\Contracts\Ip\Cidr;
use App\Contracts\Ip\Dns;
use App\Contracts\Ip\Ping;
use App\Contracts\Nmap\Nmap;
use App\Contracts\Storage\Ip;
use App\Observers\HistoryObserver;
use App\Services\Ip\Dns\PhpDns;
use App\Services\Ip\Ping\PhpPing;
use App\Services\Nmap\Discover;
use App\Services\Nmap\Parsers\Greppable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindCidr();
        $this->bindDns();
        $this->bindPing();
        $this->bindNmap();
        $this->bindHandler();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('subnet', 'App\Validators\Subnet@validate');
        $this->registerHistoryObserver();
    }

    protected function bindCidr()
    {
        $this->app->bind(
            Cidr::class,
            \App\Services\Ip\Cidr::class
        );
    }

    protected function bindDns()
    {
        $this->app->bind(
            Dns::class,
            function ($app) {
                $servers = $app['config']->get('dns.servers', ['8.8.8.8']);
                return new PhpDns($servers);
            }
        );
    }

    protected function bindHandler()
    {
        $this->app->bind(
            Handler::class,
            function ($app) {
                $h = $app[Nmap::class];
                return new \App\Services\Handler($h);
            }
        );
    }

    protected function bindNmap()
    {
        $this->app->bind(
            Nmap::class,
            function ($app) {
                return new Discover(new Greppable(), $app['config']->get('nmap', []));
            }
        );
    }

    protected function bindPing()
    {
        $this->app->bind(
            Ping::class,
            function ($app) {
                return new PhpPing($app[Dns::class]);
            }
        );
    }

    protected function registerHistoryObserver()
    {
        $observer = HistoryObserver::class;
        $store = $this->app[Ip::class];
        if ($store) {
            $mclass = get_class($store->getModel());
            $mclass::observe($observer);
        }
    }
}
