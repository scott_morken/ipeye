<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:38 PM
 */

namespace App\Storage\Eloquent;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class HistoricalIp extends Base implements \App\Contracts\Storage\HistoricalIp
{

    /**
     * @param \App\Contracts\Models\Ip $ip
     * @return \App\Contracts\Models\HistoricalIp
     */
    public function createFromIp(\App\Contracts\Models\Ip $ip)
    {
        $data = [
            'ip_id'            => $ip->id,
            'hostname'      => $ip->hostname,
            'last_response' => $ip->last_response,
        ];
        $m = $this->getModel()->newInstance();
        $m->fill($data);
        $m->save();
        return $m;
    }

    /**
     * @param string $ip
     * @return Collection<\App\Contracts\Models\HistoricalIp>
     */
    public function getHistoryByIpAddress($ip)
    {
        $key = 'history.' . $ip;
        return Cache::remember(
            $key,
            60 * 5,
            function () use ($ip) {
                return $this->getModel()
                            ->newQuery()
                            ->ipIs($ip)
                            ->orderBy('updated_at', 'desc')
                            ->get();
            }
        );
    }

    public function cleanup()
    {
        return $this->getModel()->isEmpty()->delete();
    }
}
