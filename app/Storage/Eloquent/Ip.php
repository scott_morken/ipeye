<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:43 PM
 */

namespace App\Storage\Eloquent;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Smorken\Support\Contracts\Filter;

class Ip extends Base implements \App\Contracts\Storage\Ip
{

    /**
     * @param $ip
     * @param $hostname
     * @param $response_time
     * @return bool
     */
    public function updateIp($ip, $hostname, $response_time)
    {
        $id = ['id' => $this->convertToLong($ip)];
        $update = $this->getUpdateData($response_time);
        if ($hostname !== false) {
            $update['hostname'] = $this->getHostname($ip, $hostname);
        }
        return $this->getModel()
            ->updateOrCreate($id, $update);
    }

    /**
     * @param  \Generator  $range
     * @return int
     */
    public function createInRange(\Generator $range)
    {
        $count = 0;
        foreach ($range as $ip) {
            $m = $this->createByIp($ip);
            if ($m) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * @param  int  $ip
     * @return \App\Contracts\Models\Ip
     */
    public function createByIp($ip)
    {
        return $this->getModel()
            ->firstOrCreate(['id' => $this->convertToLong($ip)]);
    }

    /**
     * @param  int  $start
     * @param  int  $end
     * @return Collection<\App\Contracts\Models\Ip>
     */
    public function getByLongIpBetween($start, $end)
    {
        $key = 'ips.between.'.$start.'.'.$end;
        return Cache::remember(
            $key,
            60 * 5,
            function () use ($start, $end) {
                return $this->ipQuery($start, $end)
                    ->get();
            }
        );
    }

    /**
     * @param  Filter  $filter
     * @param  int  $start
     * @param  int  $end
     * @return Collection <\App\Contracts\Models\Ip>
     */
    public function getByFilterAndLongIpBetween(Filter $filter, $start, $end)
    {
        $q = $this->ipQuery($start, $end);
        $this->addHostnameFilter($q, $filter);
        $this->addResponseFilter($q, $filter);
        return $q->get();
    }

    protected function getUpdateData($response_time)
    {
        return [
            'response' => $response_time ?: 0,
            'last_response' => $response_time ? Carbon::now() : null,
        ];
    }

    protected function getHostname($ip, $hostname)
    {
        return $hostname !== $ip ? $hostname : null;
    }

    protected function addHostnameFilter($q, Filter $filter)
    {
        if ($filter->hostname) {
            if ($filter->hostname === 'null') {
                $q->where('hostname', '<>', '');
            } else {
                $q->where('hostname', 'LIKE', '%'.$filter->hostname.'%');
            }
        }
        return $q;
    }

    protected function addResponseFilter($q, Filter $filter)
    {
        switch ($filter->response) {
            case 'none':
                $q->whereNull('last_response');
                break;
            case 'only':
                $q->whereNotNull('last_response');
                break;
            default:
                break;
        }
        return $q;
    }

    protected function ipQuery($start, $end)
    {
        return $this->getModel()
            ->newQuery()
            ->ipBetween($start, $end)
            ->with('history')
            ->orderBy('id');
    }

    protected function convertToLong($ip)
    {
        if (strpos($ip, '.') !== false) {
            return ip2long($ip);
        }
        return $ip;
    }
}
