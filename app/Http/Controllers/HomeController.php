<?php


namespace App\Http\Controllers;


use App\Contracts\Ip\Cidr;
use App\Contracts\Storage\Ip;
use App\Contracts\Storage\Subnet;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\View;
use Smorken\Ext\Controller\Traits\Model;
use Smorken\Support\Filter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HomeController extends Controller
{

    use Model;

    protected $base_view = 'home';

    /**
     * @var Subnet
     */
    protected $subnet;

    /**
     * @var Cidr
     */
    protected $cidr;

    public function __construct(Ip $ip, Cidr $cidr, Subnet $subnet)
    {
        $this->setProvider($ip);
        $this->cidr = $cidr;
        $this->subnet = $subnet;
        parent::__construct();
    }

    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        $subnet = null;
        $cidr = null;
        $models = new Collection();
        $subnets = $this->subnet->all();
        if ($filter->subnet_id) {
            $subnet = $this->findSubnetModel($filter->subnet_id);
            $cidr = $this->cidr->newInstance($subnet->subnet);
            $models = $this->getProvider()->getByFilterAndLongIpBetween(
                $filter,
                $cidr->getFirst(true),
                $cidr->getLast(true)
            );
        }
        return View::make($this->getView('index'))
            ->with('models', $models)
            ->with('subnets', $subnets)
            ->with('filter', $filter)
            ->with('subnet', $subnet)
            ->with('cidr', $cidr);
    }

    public function walk(Request $request)
    {
        $subnet = $request->get('subnet');
        if ($subnet) {
            Artisan::call('ip:walk', ['subnet' => $subnet, '--hosts']);
        }
    }

    protected function findSubnetModel($id)
    {
        $m = $this->subnet->find($id);
        if (!$m) {
            throw new NotFoundHttpException("Resource $id was not found.");
        }
        return $m;
    }

    protected function getFilter(Request $request)
    {
        return new Filter([
            'subnet_id' => $request->get('subnet_id', null),
            'hostname' => $request->get('hostname', null),
            'response' => $request->get('response', 'all'),
        ]);
    }
}