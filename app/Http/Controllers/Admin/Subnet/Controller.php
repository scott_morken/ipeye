<?php


namespace App\Http\Controllers\Admin\Subnet;


use App\Contracts\Ip\Cidr;
use App\Contracts\Storage\Ip;
use App\Contracts\Storage\Subnet;
use App\Http\Requests\Admin\SubnetForm;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;

class Controller extends \App\Http\Controllers\Controller
{

    use Full, IndexFiltered {
        doSave as traitedDoSave;
    }

    protected $base_view = 'admin.subnet';

    protected $subnav = 'admin';

    /**
     * @var Cidr
     */
    protected $cidr;

    /**
     * @var Ip
     */
    protected $ip;

    public function __construct(Subnet $subnet, Cidr $cidr, Ip $ip)
    {
        $this->setProvider($subnet);
        $this->cidr = $cidr;
        $this->ip = $ip;
        parent::__construct();
    }

    public function doSave(SubnetForm $request, $id = null)
    {
        $count = $this->handleIpCreation($request->get('subnet'));
        $request->session()->flash('flash:info', "Created/updated [$count] IP addresses.");
        return $this->traitedDoSave($request, $id);
    }

    protected function handleIpCreation($subnet)
    {
        $c = $this->cidr->newInstance($subnet);
        $count = $this->ip->createInRange($c->getRange());
        return $count;
    }
}