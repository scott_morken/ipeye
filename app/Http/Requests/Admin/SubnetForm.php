<?php


namespace App\Http\Requests\Admin;


use App\Contracts\Storage\Subnet;
use Illuminate\Foundation\Http\FormRequest;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Sanitizer\Traits\SanitizeRequest;

class SubnetForm extends FormRequest
{

    use SanitizeRequest;

    public function rules(Subnet $subnet, Sanitize $sanitize)
    {
        $this->sanitize($sanitize, $this, [
            'subnet' => function ($v) {
                return preg_replace('/[^0-9\.\/]/', '', $v);
            },
            'descr' => 'string',
        ]);
        return $subnet->validationRules();
    }
}