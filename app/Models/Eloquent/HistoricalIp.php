<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:35 PM
 */

namespace App\Models\Eloquent;

class HistoricalIp extends Base implements \App\Contracts\Models\HistoricalIp
{

    public $dates = ['created_at', 'updated_at', 'last_response'];
    protected $fillable = ['ip_id', 'hostname', 'last_response', 'response',];
    protected $rules = [
        'ip_id' => 'required',
        'response' => 'integer',
        'last_response' => 'date',
    ];

    public function getIpAttribute()
    {
        return long2ip($this->attributes['ip_id'] ?? null);
    }

    public function setIpIdAttribute($v)
    {
        $this->attributes['ip_id'] = $this->convertToLong($v);
    }

    public function scopeIpIs($q, $ip)
    {
        return $q->where('ip_id', '=', $this->convertToLong($ip));
    }

    public function scopeIsEmpty($query)
    {
        return $query->whereNull('last_response')->where('hostname', '=', '');
    }

    protected function convertToLong($ip)
    {
        if ((int) $ip != $ip) {
            return ip2long($ip);
        }
        return $ip;
    }
}
