<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:37 PM
 */

namespace App\Models\Eloquent;

use Illuminate\Database\MySqlConnection;
use Illuminate\Support\Facades\DB;
use Smorken\Model\Traits\FriendlyColumns;

class Subnet extends Base implements \App\Contracts\Models\Subnet
{

    use FriendlyColumns;

    protected $fillable = ['subnet', 'descr'];

    protected $rules = [
        'subnet' => 'required|subnet',
        'descr' => 'required',
    ];

    protected $friendly_columns = [
        'id' => 'ID',
        'subnet' => 'Subnet',
        'descr' => 'Description',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy($this->getDefaultOrder());
    }

    protected function getDefaultOrder()
    {
        $conn = $this->getConnection();
        if ($conn instanceof MySqlConnection) {
            return DB::raw('INET_ATON(SUBSTRING_INDEX(`subnet`, "/", 1))');
        }
        return 'subnet';
    }
}
