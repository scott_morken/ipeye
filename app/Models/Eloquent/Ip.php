<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:35 PM
 */

namespace App\Models\Eloquent;

class Ip extends Base implements \App\Contracts\Models\Ip
{

    public $incrementing = false;
    public $dates = ['created_at', 'updated_at', 'last_response'];
    protected $fillable = ['id', 'hostname', 'last_response', 'response'];
    protected $rules = [
        'id' => 'required',
        'response' => 'integer',
        'last_response' => 'date',
    ];

    public function history()
    {
        return $this->hasMany(HistoricalIp::class)
            ->orderBy('updated_at', 'desc');
    }

    public function getIpAttribute()
    {
        return long2ip($this->attributes['id'] ?? null);
    }

    public function setIdAttribute($v)
    {
        $this->attributes['id'] = $this->convertToLong($v);
    }

    public function scopeIpIs($q, $ip)
    {
        return $q->where('id', '=', $this->convertToLong($ip));
    }

    public function scopeIpBetween($q, $start, $stop)
    {
        $q->where('id', '>=', $this->convertToLong($start))
            ->where('id', '<=', $this->convertToLong($stop));
        return $q;
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('id');
    }

    protected function convertToLong($ip)
    {
        if ((int) $ip != $ip) {
            return ip2long($ip);
        }
        return $ip;
    }
}
