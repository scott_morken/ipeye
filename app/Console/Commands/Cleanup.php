<?php


namespace App\Console\Commands;


use App\Contracts\Storage\HistoricalIp;
use Illuminate\Console\Command;

class Cleanup extends Command
{

    protected $signature = 'ip:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean up historical IP results.';

    /**
     * @var HistoricalIp
     */
    protected $historicalIp;

    public function __construct(HistoricalIp $historicalIp)
    {
        $this->historicalIp = $historicalIp;
        parent::__construct();
    }

    public function handle()
    {
        $result = $this->historicalIp->cleanup();
        $this->info(sprintf("Removed %d historical IP records.", $result));
    }
}