<?php namespace App\Console\Commands;

use App\Contracts\Handler;
use App\Contracts\Ip\Cidr;
use App\Contracts\Storage\Ip;
use App\Contracts\Storage\Subnet;
use Illuminate\Console\Command;

class Walk extends Command
{

    protected $signature = 'ip:walk {subnet=all} {--hosts : Do hostname lookup}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Walk the subnet and update the database tables.';

    /**
     * @var Cidr
     */
    protected $cidr;

    /**
     * @var Ip
     */
    protected $ipProvider;

    /**
     * @var \App\Contracts\Ip\Ping|\App\Contracts\Ip\Walk
     */
    protected $handler;

    /**
     * @var Subnet
     */
    protected $subnetProvider;

    protected $return = 0;

    public function __construct(
        Cidr $cidr,
        Ip $ipProvider,
        Handler $handler,
        Subnet $subnetProvider

    ) {
        $this->cidr = $cidr;
        $this->ipProvider = $ipProvider;
        $this->handler = $handler->getHandler();
        $this->subnetProvider = $subnetProvider;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);
        $subnets = $this->getSubnets();
        $lookup = $this->option('hosts');
        foreach ($subnets as $subnet) {
            $cidr = $this->cidr->newInstance($subnet);
            $this->info(sprintf('Subnet: %s %s', $cidr->getIp(), $cidr->getNetmask()));
            if (extension_loaded('pthreads')) {
                $this->iterateIpsMultiThread($cidr, $lookup);
            } else {
                $this->iterateIpsSingleThread($cidr, $lookup);
            }
        }
        $this->info(sprintf('Ran for %s seconds', microtime(true) - $start));
        return $this->return;
    }

    protected function getSubnets()
    {
        $subnets = [];
        $subnet = $this->argument('subnet');
        if ($subnet === 'all') {
            $models = $this->subnetProvider->all();
            foreach ($models as $model) {
                $subnets[] = $model->subnet;
            }
        } else {
            $subnets[] = $subnet;
        }
        return $subnets;
    }

    protected function handleResult(array $result)
    {
        $this->info(
            sprintf(
                '%s [%s] took %s ms',
                $result['ip'],
                $result['hostname'],
                $result['result'] !== false ? $result['result'] : 'N/A'
            )
        );
        if ($result['error']) {
            $this->return = 1;
            $this->error('Error: ' . $result['error']);
        } else {
            $this->storeResult($result['ip'], $result['hostname'], $result['result']);
            return true;
        }
    }

    protected function iterateIpsMultiThread(Cidr $cidr, $lookup)
    {
        //Not currently supported.. some-a-day
        $this->iterateIpsSingleThread($cidr, $lookup);
    }

    protected function iterateIpsSingleThread(Cidr $cidr, $lookup)
    {
        if ($this->handler instanceof \App\Contracts\Ip\Ping) {
            foreach ($cidr->getRange() as $ip) {
                $this->pingIp($ip, $lookup);
            }
        } elseif ($this->handler instanceof \App\Contracts\Ip\Walk) {
            $this->walkCidr($cidr, $lookup);
        }
    }

    protected function pingIp($ip, $lookup)
    {
        $results = $this->handler->ping($ip, $lookup);
        foreach ($results as $result) {
            if ($this->handleResult($result)) {
                break;
            }
        }
    }

    protected function storeResult($ip, $hostname, $response_time)
    {
        $this->ipProvider->updateIp($ip, $hostname, $response_time);
    }

    protected function walkCidr(Cidr $cidr, $lookup)
    {
        $results = $this->handler->walk($cidr, $lookup);
        if ($results) {
            foreach ($results as $ip => $ip_results) {
                foreach ($ip_results as $result) {
                    if ($this->handleResult($result)) {
                        break;
                    }
                }
            }
        } else {
            $this->error('Error walking CIDR');
        }
    }
}
