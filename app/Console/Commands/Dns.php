<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class Dns extends Command
{

    protected $signature = 'ip:dns {ip_address} {dns_server?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Look up an IP address hostname';

    /**
     * @var \App\Contracts\Ip\Dns
     */
    protected $dns;

    public function __construct(\App\Contracts\Ip\Dns $dns)
    {
        $this->dns = $dns;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $ip = $this->argument('ip_address');
        $this->info($ip);
        foreach ($this->getServers() as $server) {
            $this->lookup($ip, $server);
        }
        return 0;
    }

    protected function lookup($ip, $server)
    {
        $r = $this->dns->byIp($ip, $server);
        if ($r && $r !== $ip) {
            $this->info(sprintf('Lookup result on [%s]: %s', $server, $r));
            return true;
        }
        return false;
    }

    protected function getServers()
    {
        $servers = $this->argument('dns_server');
        if ($servers) {
            $servers = (array)$servers;
        } else {
            $servers = Config::get('dns.servers', ['8.8.8.8']);
        }
        return $servers;
    }
}
