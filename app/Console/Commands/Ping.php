<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

class Ping extends Command
{

    protected $signature = 'ip:ping {ip_address} {count=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ping an IP address';

    /**
     * @var \App\Contracts\Ip\Ping
     */
    protected $ping;

    public function __construct(\App\Contracts\Ip\Ping $ping)
    {
        $this->ping = $ping;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $results = $this->ping->ping($this->argument('ip_address'), true, $this->argument('count'));
        foreach ($results as $result) {
            $this->info(
                sprintf(
                    '%s [%s] took %s ms',
                    $result['ip'],
                    $result['hostname'],
                    $result['result'] !== false ? $result['result'] : 'N/A'
                )
            );
            if ($result['error']) {
                $this->error('Error: ' . $result['error']);
            }
        }
    }
}
