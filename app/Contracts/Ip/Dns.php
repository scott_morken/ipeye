<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/15/17
 * Time: 9:22 AM
 */

namespace App\Contracts\Ip;

interface Dns
{

    /**
     * @param string $ip IP address to lookup
     * @param int $timeout
     * @return null|string
     */
    public function byIp($ip, $timeout = 1);

    /**
     * @param array $servers
     * @return mixed
     */
    public function setDnsServers(array $servers = ['8.8.8.8']);

    /**
     * @return array
     */
    public function getDnsServers();
}
