<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/18
 * Time: 8:41 AM
 */

namespace App\Contracts\Ip;

interface Walk
{

    /**
     * @param \App\Contracts\Ip\Cidr $cidr
     * @param bool                   $lookup_host
     * @return array|false
     */
    public function walk(Cidr $cidr, $lookup_host = false);
}
