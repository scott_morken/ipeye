<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/15/17
 * Time: 6:34 AM
 */

namespace App\Contracts\Ip;

interface Ping
{

    public function ping($ip, $lookup_host = false, $count = 1);
}
