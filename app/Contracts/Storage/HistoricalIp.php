<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:31 PM
 */

namespace App\Contracts\Storage;

use Illuminate\Support\Collection;

interface HistoricalIp
{

    /**
     * @return int
     */
    public function cleanup();

    /**
     * @param  \App\Contracts\Models\Ip  $ip
     * @return \App\Contracts\Models\HistoricalIp
     */
    public function createFromIp(\App\Contracts\Models\Ip $ip);

    /**
     * @param  string  $ip
     * @return Collection<\App\Contracts\Models\HistoricalIp>
     */
    public function getHistoryByIpAddress($ip);
}
