<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:31 PM
 */

namespace App\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

interface Ip
{

    /**
     * $range is a iterable of long int IPs
     * @param  \Generator  $range
     * @return int
     */
    public function createInRange(\Generator $range);

    /**
     * @param  int  $ip
     * @return \App\Contracts\Models\Ip
     */
    public function createByIp($ip);

    /**
     * @param  int  $start
     * @param  int  $end
     * @return Collection<\App\Contracts\Models\Ip>
     */
    public function getByLongIpBetween($start, $end);

    /**
     * @param  Filter  $filter
     * @param  int  $start
     * @param  int  $end
     * @return Collection <\App\Contracts\Models\Ip>
     */
    public function getByFilterAndLongIpBetween(Filter $filter, $start, $end);

    /**
     * @param $ip
     * @param $hostname
     * @param $response_time
     * @return bool
     */
    public function updateIp($ip, $hostname, $response_time);
}
