<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:26 PM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Subnet
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property
 */
interface Subnet extends Model
{

}
