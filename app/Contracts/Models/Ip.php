<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:25 PM
 */

namespace App\Contracts\Models;

use Illuminate\Support\Collection;
use Smorken\Model\Contracts\Model;

/**
 * Interface Ip
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property string $ip
 * @property string $hostname
 * @property \DateTime $last_response
 * @property int $response
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Collection<\App\Contracts\Models\HistoricalIp> $history
 */
interface Ip extends Model
{

}
