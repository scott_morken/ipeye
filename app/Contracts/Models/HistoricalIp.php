<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/14/17
 * Time: 1:29 PM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface HistoricalIp
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property int $ip_id
 * @property string $ip
 * @property string $hostname
 * @property \DateTime $last_response
 * @property int $response
 */
interface HistoricalIp extends Model
{

}
