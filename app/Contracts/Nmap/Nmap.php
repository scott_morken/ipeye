<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/18
 * Time: 8:34 AM
 */

namespace App\Contracts\Nmap;

use App\Contracts\Ip\Walk;

interface Nmap extends Walk
{

    /**
     * @return \App\Contracts\Nmap\Response
     */
    public function getResponseParser();

    /**
     * @param \App\Contracts\Nmap\Response $response
     * @return void
     */
    public function setResponseParser(Response $response);
}
