<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/18
 * Time: 8:35 AM
 */

namespace App\Contracts\Nmap;

interface Response
{

    /**
     * @param $response
     * @return array
     */
    public function parse($response);

    /**
     * @param $response
     * @return bool
     */
    public function isError($response);
}
