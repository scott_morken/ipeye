<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/15/17
 * Time: 10:06 AM
 */

namespace App\Validators;

use App\Contracts\Ip\Cidr;

class Subnet
{

    /**
     * @var Cidr
     */
    protected $cidr;

    /**
     * Subnet constructor.
     * @param Cidr $cidr
     */
    public function __construct(Cidr $cidr)
    {
        $this->cidr = $cidr;
    }

    public function validate($attribute, $value, $parameters, $validator)
    {
        try {
            $cidr = $this->cidr->newInstance($value);
            return true;
        } catch (\InvalidArgumentException $e) {
            return false;
        }
    }
}
