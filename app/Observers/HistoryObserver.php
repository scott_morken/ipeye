<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/15/17
 * Time: 12:38 PM
 */

namespace App\Observers;

use App\Contracts\Storage\HistoricalIp;
use Carbon\Carbon;

class HistoryObserver
{

    protected $history;

    public function __construct(HistoricalIp $history)
    {
        $this->history = $history;
    }

    public function updating($model)
    {
        if ($this->shouldUpdate($model)) {
            if ($this->needsHistory($model)) {
                $this->createHistory($model);
            }
            return true;
        }
        return false;
    }

    protected function shouldUpdate($model, $seconds = 3600)
    {
        $current = $model->last_response;
        $previous = $model->getOriginal('last_response');
        if (!$current && $previous) {
            $now = Carbon::now();
            $previous = $this->toDate($previous);
            return $now->diffInSeconds($previous) >= $seconds;
        }
        return true;
    }

    protected function needsHistory($model)
    {
        if ($this->isNew($model)) {
            return false;
        }
        if ($this->hasHostnameChange($model)) {
            return true;
        }
        if ($this->hasNewResponse($model)) {
            return true;
        }
        return false;
    }

    protected function createHistory($model)
    {
        $m = $this->history->getModel()->newInstance();
        $m->fill(
            [
                'ip_id'         => $model->id,
                'hostname'      => $model->getOriginal('hostname'),
                'last_response' => $model->getOriginal('last_response'),
                'response'      => $model->getOriginal('response'),
            ]
        );
        return $m->save();
    }

    protected function isNew($model)
    {
        return ($model->getOriginal('created_at') === $model->getOriginal('updated_at') ||
            $this->checkTimeDiffLessThan($model, 5));
    }

    protected function checkTimeDiffLessThan($model, $seconds = 3600)
    {
        $created = $model->getOriginal('created_at');
        $updated = $model->getOriginal('updated_at');
        if ($created === $updated) {
            return true;
        }
        $created = $this->toDate($created);
        $updated = $this->toDate($updated);
        return ($updated->diffInSeconds($created) <= $seconds);
    }

    protected function hasNewResponse($model)
    {
        $current = $model->last_response;
        $previous = $model->getOriginal('last_response');
        return $current && !$previous;
    }

    protected function hasHostnameChange($model, $seconds = 3600)
    {
        $current = $model->hostname;
        $previous = $model->getOriginal('hostname');
        if ($current != $previous) {
            return true;
        }
        return false;
    }

    protected function toDate($d)
    {
        return is_object($d) ? $d : Carbon::parse($d);
    }
}
