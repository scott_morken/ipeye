<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Eloquent\HistoricalIp::class, function (Faker $faker) {
    return [
        'ip_id' => ip2long($faker->ipv4),
        'response' => $faker->randomNumber(3),
        'last_response' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(\App\Models\Eloquent\Ip::class, function (Faker $faker) {
    return [
        'id' => ip2long($faker->ipv4),
        'response' => $faker->randomNumber(3),
        'last_response' => date('Y-m-d H:i:s'),
    ];
});

$factory->define(\App\Models\Eloquent\Subnet::class, function (Faker $faker) {
    return [
        'subnet' => '127.0.0.0/24',
        'descr' => $faker->words(3, true),
    ];
});