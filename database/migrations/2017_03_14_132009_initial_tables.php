<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class InitialTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'ips',
            function (Blueprint $t) {
                $t->bigInteger('id');
                $t->string('hostname', 128)->default('');
                $t->dateTime('last_response')->nullable();
                $t->decimal('response', 8, 4)->default(0);
                $t->timestamps();

                $t->primary('id');
            }
        );

        Schema::create(
            'historical_ips',
            function (Blueprint $t) {
                $t->increments('id');
                $t->bigInteger('ip_id');
                $t->string('hostname', 128)->default('');
                $t->dateTime('last_response')->nullable();
                $t->decimal('response', 8, 4)->default(0);
                $t->timestamps();

                $t->index('ip_id', 'hip_ip_id_ndx');
                $t->index('last_response', 'hip_last_response_ndx');
            }
        );

        Schema::create(
            'subnets',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('subnet', 20);
                $t->string('descr', 64);
                $t->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = [
            'ips',
            'historical_ips',
            'subnets',
        ];
        foreach ($tables as $t) {
            Schema::drop($t);
        }
    }
}
