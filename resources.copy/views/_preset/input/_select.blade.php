<select class="form-control {{ $classes??'' }}" name="{{ $name }}" id="{{ $id??$name }}">
        <?php $value = $value??old($name, (isset($model) && $model->$name) ? $model->$name : null); ?>
        @foreach ($items as $k => $v)
                <option value="{{ $k }}" {{ (!is_null($value) && $value == $k) ? 'selected' : '' }}>{{ $v }}</option>
        @endforeach
</select>
