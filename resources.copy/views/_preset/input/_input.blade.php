<input type="{{ $type??'text' }}" name="{{ $name }}" id="{{ $id??$name }}" class="form-control {{ $classes??'' }}"
       value="{{ $value??old($name, (isset($model) && $model->$name) ? $model->$name : null) }}" placeholder="{{ $placeholder??'' }}"
       maxlength="{{ $maxlength??255 }}">
