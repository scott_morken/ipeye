<?php if (!isset($dir)) $dir = 'up'; ?>
@if ($ip->last_response)
    <?php $diff = $ip->last_response->diffInDays(); ?>
    @if ($diff < 1)
        <i class="fa fa-arrow-circle-{{ $dir }} text-up"></i>
    @elseif ($diff >= 1 && $diff <= 14)
        <i class="fa fa-arrow-circle-{{ $dir }} text-down"></i>
    @else
        <i class="fa fa-arrow-circle-{{ $dir }} text-down-old"></i>
    @endif
@else
    @if (!$ip->history || ($ip->history && count($ip->history) === 0))
        <i class="fa fa-arrow-circle-down text-muted"></i>
    @elseif ($ip->history && count($ip->history))
        <?php $last = $ip->history->first(); ?>
        @include('home._icon', ['ip' => $last, 'dir' => 'down'])
    @endif
@endif
