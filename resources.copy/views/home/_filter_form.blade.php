<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="form-inline" method="get">
            <div class="form-group mb-2 mr-2">
                @include('_preset.input._label', ['name' => 'subnet_id', 'title' => 'Subnet', 'label_classes' => 'sr-only'])
                @include('_preset.input._select', [
                'name' => 'subnet_id',
                'classes' => $filter->subnet_id ? $filtered : '',
                'value' => $filter->subnet_id,
                'items' => ['' => '-- Select One --'] + $subnets->pluck('descr', 'id')->all()
                ])
            </div>
            <div class="form-group mb-2 mr-2">
                @include('_preset.input._label', ['name' => 'hostname', 'title' => 'Hostname', 'label_classes' => 'sr-only'])
                @include('_preset.input._input', [
                'name' => 'hostname',
                'classes' => $filter->hostname ? $filtered : '',
                'value' => $filter->hostname,
                'placeholder' => 'hostname'
                ])
            </div>
            <div class="form-group mb-2 mr-2">
                @include('_preset.input._label', ['name' => 'response', 'title' => 'Response', 'label_classes' => 'sr-only'])
                @include('_preset.input._select', [
                'name' => 'response',
                'classes' => $filter->response && $filter->response !== 'all' ? $filtered : '',
                'value' => $filter->response,
                'items' => ['all' => 'All', 'none' => 'No Response', 'only' => 'Response']
                ])
            </div>
            <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
            <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
        </form>
    </div>
</div>
