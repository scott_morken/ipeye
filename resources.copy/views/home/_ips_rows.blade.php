<div class="row header">
    <div class="col-sm-2">
        IP ({{ count($models) }})
    </div>
    <div class="col-sm-3">
        Hostname
    </div>
    <div class="col-sm-4">
        Response
    </div>
    <div class="col-sm-2">
        Updated
    </div>
    <div class="col-sm-1">
        History
    </div>
</div>
<div class="ips">
    @foreach($models as $ip)
        <div class="ip striped p-1">
            <div class="row">
                <div class="col-sm-2">
                    @include('home._icon')
                    {{ $ip->ip }}
                </div>
                <div class="col-sm-3">
                    {{ $ip->hostname ?: '--' }}
                </div>
                <div class="col-sm-4">
                    @if ($ip->last_response)
                        {{ $ip->last_response }} :: {{ $ip->response }} ms
                    @else
                        --
                    @endif
                </div>
                <div class="col-sm-2">{{ $ip->updated_at }}</div>
                <div class="col-sm-1">
                    @if ($ip->history && count($ip->history))
                        <a class="" data-toggle="collapse" href="#history-{{ $ip->id }}" aria-expanded="false"
                           aria-controls="history-{{ $ip->id }}">
                            History
                        </a>
                    @else
                        --
                    @endif
                </div>
            </div>
            @if ($ip->history && count($ip->history))
                <div class="collapse history small" id="history-{{ $ip->id }}">
                    @include('home._history', ['history' => $ip->history])
                </div>
            @endif
        </div>
    @endforeach
</div>
