<div>
    <h3>{{ $subnet->descr }} <small>[{{ $subnet->subnet }}]</small></h3>
    <dl class="dl-horizontal">
        <dt>Network</dt>
        <dd>{{ $cidr->getNetwork() }}</dd>
        <dt>Netmask</dt>
        <dd>{{ $cidr->getNetmask() }}</dd>
        <dt>Broadcast</dt>
        <dd>{{ $cidr->getBroadcast() }}</dd>
    </dl>
</div>
<hr/>
