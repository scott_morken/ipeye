@foreach($history as $hip)
    <div class="row">
        <div class="col-sm-3 offset-sm-2">
            {{ $hip->hostname ?: '--' }}
        </div>
        <div class="col-sm-4">
            @if ($hip->last_response)
                {{ $hip->last_response }} :: {{ $hip->response }} ms
            @else
                --
            @endif

        </div>
        <div class="col-sm-2">
            {{ $hip->updated_at }}
        </div>
    </div>
@endforeach