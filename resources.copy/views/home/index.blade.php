@extends('layouts.app')
@section('content')
    @include('home._filter_form')
    @if ($subnet)
        @include('home._subnet_view')
        @if ($models && count($models))
            @include('home._ips_rows')
        @else
            <div class="text-muted">No IP addresses found.</div>
        @endif
    @else
        <div class="text-muted">Please select a subnet first.</div>
    @endif
@endsection
