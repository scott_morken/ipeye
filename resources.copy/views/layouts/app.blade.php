<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @if (file_exists(public_path('favicon.ico')))
        <link rel="icon" href="{{ asset('favicon.ico') }}" sizes="16x16 32x32 48x48 64x64"
              type="image/vnd.microsoft.icon">
    @endif
    @section('scripts_head')
    <!-- Scripts -->
    @show
    @section('styles')
    <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @show
    @stack('add_to_head')
</head>
<body>
<div id="app">
    <div class="loading"></div>
    <nav class="navbar navbar-expand-md navbar-dark shadow-sm" style="background-color: #002b5e;">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                @if (file_exists(public_path('images/logo.png')))
                    <img src="{{ asset('images/logo.png') }}" alt="Logo">
                @endif
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    @guest
                        @includeIf('layouts.menus.guest')
                    @else
                        @includeIf('layouts.menus.auth')
                    @endguest
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @else
                        @includeIf('layouts.menus.manage')
                        @includeIf('layouts.menus.admin')
                        <li class="nav-item dropdown" id="logout-container">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ \Illuminate\Support\Facades\Auth::user()->shortName }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    @includeIf('layouts.menus.submenu')
    @includeIf('layouts._partials._flash')
    @if ($errors->any())
        <div class="alert alert-danger mt-2 mb-2">
            <ul class="container">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @isset($template)
        @includeFirst([$template, 'layouts.template._container'])
    @else
        @include('layouts.templates._container')
    @endif
</div>
@if (file_exists(public_path('js/app.js')))
    <script src="{{ asset('js/app.js') }}"></script>
@endif
@stack('add_to_end')
</body>
</html>
