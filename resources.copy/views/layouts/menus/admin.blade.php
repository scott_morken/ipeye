@can('role-admin')
    <?php $menus = Menu::getMenusByKey('role-admin'); ?>
    @if ($menus)
        <li class="nav-item dropdown">
            <a id="navbarAdminDropdown"
               class="nav-link dropdown-toggle {{ Menu::isActiveChain($controller, $menus) ? 'active' : null }}"
               href="#" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false" v-pre>
                Admin <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarAdminDropdown">
                @foreach($menus as $menu)
                    <?php $active = Menu::isActiveChain($controller, $menu); ?>
                    <?php $sub = $active && count($menu->children) ? $menu->children : null; ?>
                    <a class="dropdown-item {{ $active ? 'active' : null }}"
                       href="{{ action($menu->action) }}">{{ $menu->name }}</a>
                @endforeach
            </div>
        </li>
    @endif
@endcan
