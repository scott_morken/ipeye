<?php
$sub = isset($sub) ? $sub : null;
$next = null;
?>
@if ($sub)
    <nav class="sub-menu mb-1">
        <ul class="nav nav-pills">
            @foreach ($sub as $menu)
                <?php $active = Menu::isActiveChain($controller, $menu); ?>
                @if ($active && count($menu->children))
                    <?php $next = $menu->children; ?>
                @endif
                @include('layouts.menus._menu_item')
            @endforeach
        </ul>
    </nav>
@endif
@if ($next)
    @include('layouts.menus.submenu', ['sub' => $next])
@endif
