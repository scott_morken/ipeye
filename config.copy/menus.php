<?php

use Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller;

return [
    'guest' => [],
    'auth' => [],

    'role-manage' => [],
    'role-admin' => [
        [
            'name' => 'Subnets',
            'action' => [\App\Http\Controllers\Admin\Subnet\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Users',
            'action' => [Controller::class, 'index'],
            'children' => [],
        ],
    ],
];
