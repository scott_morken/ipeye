<?php
return [
    'concrete' => [
        \App\Storage\Eloquent\HistoricalIp::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\HistoricalIp::class,
            ],
        ],
        \App\Storage\Eloquent\Ip::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Ip::class,
            ],
        ],
        \App\Storage\Eloquent\Subnet::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Subnet::class,
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\HistoricalIp::class => \App\Storage\Eloquent\HistoricalIp::class,
        \App\Contracts\Storage\Ip::class => \App\Storage\Eloquent\Ip::class,
        \App\Contracts\Storage\Subnet::class => \App\Storage\Eloquent\Subnet::class,
    ],
];
